//
//  DecoderVideo.h
//  TestFFmpeg
//
//  Created by leeson zhong on 2020/7/24.
//  Copyright © 2020 octant. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DecoderVideo : NSObject

-(int)decodeVideo:(NSString *) videoPath;

@end

NS_ASSUME_NONNULL_END
