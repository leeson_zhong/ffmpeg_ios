//
//  DecoderVideo.m
//  TestFFmpeg
//
//  Created by leeson zhong on 2020/7/24.
//  Copyright © 2020 octant. All rights reserved.
//

#import "DecoderVideo.h"
#import "FrameData.h"
#import "VideoInfo.h"

#include "libavformat/avformat.h"
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>

@implementation DecoderVideo

AVFormatContext *fmt_ctx;
AVCodecContext *dec_ctx;
int video_stream_index = -1;
-(int)open_input_file:(const char *)filename
{
    int ret;
    AVCodec *dec;
    NSLog(@"filename = %s",filename);
    if ((ret = avformat_open_input(&fmt_ctx, filename, NULL, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open input file\n");
        return ret;
    }

    if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
        return ret;
    }

    /* select the video stream */
    ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find a video stream in the input file\n");
        return ret;
    }
    video_stream_index = ret;

    /*看一下log，已经默认调用h264_mediacodec了，不加这段代码也没事
    应该是av_find_best_stream函数帮忙找到最好的解码器
   switch (dec->id){

        case AV_CODEC_ID_H264:{
            dec = avcodec_find_decoder_by_name("h264_mediacodec");
            break;
        }

        case AV_CODEC_ID_MPEG4:{
            dec = avcodec_find_decoder_by_name("mpeg4_mediacodec");
            break;
        }

        default:{
            break;
        }

    }*/
    /* create decoding context */
    dec_ctx = avcodec_alloc_context3(dec);
    if (!dec_ctx)
        return AVERROR(ENOMEM);

    avcodec_parameters_to_context(dec_ctx, fmt_ctx->streams[video_stream_index]->codecpar);
    av_opt_set_int(dec_ctx, "refcounted_frames", 1, 0);

    /* init the video decoder */
    if ((ret = avcodec_open2(dec_ctx, dec, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
        return ret;
    }

    av_log(NULL, AV_LOG_INFO, "dec_ctx->framerate.num=%d,den=%d,dec_ctx->time_base.num=%d,den=%d\n",dec_ctx->framerate.num,dec_ctx->framerate.den,dec_ctx->time_base.num,dec_ctx->time_base.den);
    NSLog(@"decode codec name=%s,codec id=%d,long_name=%s,wrapper_name=%s",dec->name,dec->id,dec->long_name,dec->wrapper_name);
    
    /*for (int i=0; i<100; i++) {
        const AVCodecHWConfig * config = avcodec_get_hw_config(dec, i);
        if (config) {
            NSLog(@"AVCodecHWConfig device_type=%d,pix_fmt=%d",config->device_type,config->pix_fmt);
        }else{
            //NSLog(@"AVCodecHWConfig is null");
        }
    }*/
    [VideoInfo shareVideoInfo].fmt_ctx = fmt_ctx;
    [VideoInfo shareVideoInfo].dec_ctx = dec_ctx;
    [VideoInfo shareVideoInfo].video_stream_index = video_stream_index;
    return 0;
}

-(int)decodeVideo:(NSString *) videoPath{
    
    
    //输出ffmpeg的log信息
    //av_log_set_level(AV_LOG_WARNING);
    
    int ret;
    AVPacket packet;
    AVFrame *frame = av_frame_alloc();
    if (!frame ) {
        av_log(NULL, AV_LOG_ERROR, "Cannot alloc frame\n");
        return -1;
    }
    //is deprecated
    //av_register_all();

    uint8_t *yuvData = NULL;
    int frameCount = 0;

    if ((ret = [self open_input_file:[videoPath cStringUsingEncoding:NSUTF8StringEncoding]]) < 0)
        goto end;
    
    yuvData = (uint8_t*)malloc(sizeof(uint8_t) * dec_ctx->width*dec_ctx->height*3/2);
    /* read all packets */
    [FrameData shareFrameData].decodeStatus = 1;
    [[FrameData shareFrameData] initYuvDataQueueNum:10 Len:dec_ctx->width*dec_ctx->height*3/2];
    while (1) {
        if ((ret = av_read_frame(fmt_ctx, &packet)) < 0)
            break;

        if (packet.stream_index == video_stream_index) {
            ret = avcodec_send_packet(dec_ctx, &packet);
            if (ret < 0) {
                av_log(NULL, AV_LOG_ERROR, "Error while sending a packet to the decoder\n");
                break;
            }

            while (ret >= 0) {
                ret = avcodec_receive_frame(dec_ctx, frame);
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                    break;
                } else if (ret < 0) {
                    av_log(NULL, AV_LOG_ERROR, "Error while receiving a frame from the decoder\n");
                    goto end;
                }

                if (ret >= 0) {
                    //av_log(NULL, AV_LOG_INFO, "decode a frame success\n");
                    NSLog(@"decode a frame success,frameCount=%d,frame pixformat=%d,videotoolbox=%d,yuv420p=%d,n12=%d,n21=%d,frame->pict_type=%d",frameCount,frame->format,AV_PIX_FMT_VIDEOTOOLBOX,AV_PIX_FMT_YUV420P,AV_PIX_FMT_NV12,AV_PIX_FMT_NV21,frame->pict_type);
                    frameCount++;
                     
                    //[self convertAVFrameToYuvData:frame YuvData:yuvData];
                    if ([FrameData shareFrameData].encode) {
//                        while ([[FrameData shareFrameData] offerYuvData:yuvData Len:dec_ctx->width*dec_ctx->height*3/2] != YES) {
//                            [NSThread sleepForTimeInterval:0.01];
//                        }
                        while (true) {
                            NSMutableData *data = [[FrameData shareFrameData] offerYuvData];
                            if (data) {
                                [self convertAVFrameToYuvData:frame YuvData:[data mutableBytes]];
                                break;
                            }
                            [NSThread sleepForTimeInterval:0.01];
                        }
                    }

                    av_frame_unref(frame);
                }
            }
        }
        av_packet_unref(&packet);
    }
end:
    avcodec_free_context(&dec_ctx);
    avformat_close_input(&fmt_ctx);
    av_frame_free(&frame);
    if (yuvData != NULL) {
        free(yuvData);
    }
    [FrameData shareFrameData].decodeStatus = 2;
    return 0;
}


-(void) convertAVFrameToYuvData:(AVFrame *)pFrame YuvData:(uint8_t *)yuvData
{
    if(pFrame->format == AV_PIX_FMT_YUV420P){//yyyyyyyy uu vv
        int t_frameWidth = pFrame->width;
        int t_frameHeight = pFrame->height;
        int t_yPerRowBytes = pFrame->linesize[0];
        int t_uPerRowBytes = pFrame->linesize[1];
        int t_vPerRowBytes = pFrame->linesize[2];

        for(int i = 0;i< t_frameHeight ;i++)
        {
            memcpy(yuvData+i*t_frameWidth,(char*)(pFrame->data[0]+i*t_yPerRowBytes),t_frameWidth);
        }

        for(int i = 0;i< t_frameHeight/2 ;i++)
        {
            memcpy(yuvData+t_frameHeight*t_frameWidth+i*t_frameWidth/2,(char*)(pFrame->data[1]+i*t_uPerRowBytes),t_frameWidth/2);
        }

        for(int i = 0;i< t_frameHeight/2 ;i++)
        {
            memcpy(yuvData+t_frameHeight*t_frameWidth*5/4+i*t_frameWidth/2,(pFrame->data[2]+i*t_vPerRowBytes),t_frameWidth/2);
        }

    }else if(pFrame->format == AV_PIX_FMT_NV12){//yyyyyyyy uv uv
         int t_frameWidth = pFrame->width;
         int t_frameHeight = pFrame->height;
         int t_yPerRowBytes = pFrame->linesize[0];
         int t_uvPerRowBytes = pFrame->linesize[1];


         for(int i = 0;i< t_frameHeight ;i++)
         {
             memcpy(yuvData+i*t_frameWidth,(char*)(pFrame->data[0]+i*t_yPerRowBytes),t_frameWidth);
         }

         for(int i = 0;i< t_frameHeight/2 ;i++)
         {
             memcpy(yuvData+t_frameHeight*t_frameWidth+i*t_frameWidth,(char*)(pFrame->data[1]+i*t_uvPerRowBytes),t_frameWidth);
         }


        }else if(pFrame->format == AV_PIX_FMT_NV21){//yyyyyyyy vu vu
          int t_frameWidth = pFrame->width;
          int t_frameHeight = pFrame->height;
          int t_yPerRowBytes = pFrame->linesize[0];
          int t_uvPerRowBytes = pFrame->linesize[1];

          for(int i = 0;i< t_frameHeight ;i++)
          {
              memcpy(yuvData+i*t_frameWidth,(char*)(pFrame->data[0]+i*t_yPerRowBytes),t_frameWidth);
          }

          for(int i = 0;i< t_frameHeight/2 ;i++)
          {
              memcpy(yuvData+t_frameHeight*t_frameWidth+i*t_frameWidth,(char*)(pFrame->data[1]+i*t_uvPerRowBytes),t_frameWidth);
          }

        }

}


@end
