//
//  ViewController.m
//  TestFFmpeg
//
//  Created by leeson zhong on 2020/7/23.
//  Copyright © 2020 octant. All rights reserved.
//

#import "ViewController.h"
#import "ffversion.h"
#import "DecoderVideo.h"
#import "EncoderVideo.h"
#import "VideoInfo.h"
#import "FrameData.h"
#import "RtspPlayer.h"
#import "RtmpUploader.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.messageLabel.text = [NSString stringWithFormat:@"FFmpeg version:%s",FFMPEG_VERSION];
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"MOVI0001" ofType:@"mov"];
//    DecoderVideo *decoderVideo = [[DecoderVideo alloc] init];
//    [decoderVideo decodeVideo:path];
    
    
    
    
    
    [FrameData shareFrameData].decodeStatus =0;
    [FrameData shareFrameData].encode = YES;

    dispatch_queue_t queue = dispatch_queue_create("net.bujige.testQueue", DISPATCH_QUEUE_CONCURRENT);
    // 异步执行任务创建方法
    dispatch_async(queue, ^{
        // 这里放异步执行任务代码
//        NSString *path = [[NSBundle mainBundle] pathForResource:@"MOVI0001" ofType:@"mov"];
//        DecoderVideo *decoderVideo = [[DecoderVideo alloc] init];
//        [decoderVideo decodeVideo:path];
        NSString *m_strServerAddress = @"0:0:0:0:0:ffff:c0a8:1901";
        NSString *videoURL = [NSString stringWithFormat:@"%@%@%@",@"rtsp://[",m_strServerAddress,@"]:8080/?action=stream"];
        while (true) {
            [[[RtspPlayer alloc] init] startPlay:videoURL];
            [NSThread sleepForTimeInterval:0.1];
        }
    });
    // 异步执行任务创建方法
    dispatch_async(queue, ^{
        // 这里放异步执行任务代码
//        NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
//        NSString *encodeVideoPath = [NSString stringWithFormat:@"%@/encode.mov",documentsPath];
//        while ([FrameData shareFrameData].decodeStatus != 1) {
//            [NSThread sleepForTimeInterval:0.01];
//        }
//        EncoderVideo *encoderVideo = [[EncoderVideo alloc] init];
//        VideoInfo *videoInfo = [VideoInfo shareVideoInfo];
//        [encoderVideo setDecoderAVFormatContext:videoInfo.fmt_ctx AVCodecContext:videoInfo.dec_ctx VideoStreamIndex:videoInfo.video_stream_index];
//        [encoderVideo open_output_file:[encodeVideoPath cStringUsingEncoding:NSUTF8StringEncoding]];
//        AVFrame *frame = [encoderVideo alloc_frame:videoInfo.dec_ctx->pix_fmt wdith:videoInfo.dec_ctx->width height:videoInfo.dec_ctx->height];
//
//        int frame_count = 0;
//        while (true) {
//            NSMutableData *data = [[FrameData shareFrameData] pollYuvData];
//            if (data) {
//                BOOL ret = [encoderVideo fillYuvDataToAVFrame:frame YuvData:[data mutableBytes] Pts:frame_count];
//                if (ret == NO) {
//                    NSLog(@"fillYuvDataToAVFrame is error");
//                }
//                [encoderVideo encode_write_frame:frame];
//                frame_count++;
//
//            }else{
//                if ([FrameData shareFrameData].decodeStatus == 2) {
//                    [encoderVideo encode_write_frame:NULL];
//                    break;
//                }
//                [NSThread sleepForTimeInterval:0.01];
//            }
//        }
//        [encoderVideo close_output_video];
        
        
        NSString *url = @"rtmp://live-push.bilivideo.com/live-bvc/";
        NSString *code = @"?streamname=live_5608461_8038897&key=8838918f6f3319a1cf84597d34cdf1e6";
        url = [NSString stringWithFormat:@"%@%@",url,code];
        while ([FrameData shareFrameData].decodeStatus != 1) {
           [NSThread sleepForTimeInterval:0.01];
        }
        RtmpUploader *encoderVideo = [[RtmpUploader alloc] init];
        VideoInfo *videoInfo = [VideoInfo shareVideoInfo];
        [encoderVideo setDecoderAVFormatContext:videoInfo.fmt_ctx AVCodecContext:videoInfo.dec_ctx VideoStreamIndex:videoInfo.video_stream_index];
        [encoderVideo open_output_file:[url cStringUsingEncoding:NSUTF8StringEncoding]];
        AVFrame *frame = [encoderVideo alloc_frame:videoInfo.dec_ctx->pix_fmt wdith:videoInfo.dec_ctx->width height:videoInfo.dec_ctx->height];

        int frame_count = 0;
        while (true) {
           NSMutableData *data = [[FrameData shareFrameData] pollYuvData];
           if (data) {
               BOOL ret = [encoderVideo fillYuvDataToAVFrame:frame YuvData:[data mutableBytes] Pts:frame_count];
               if (ret == NO) {
                   NSLog(@"fillYuvDataToAVFrame is error");
               }
               [encoderVideo encode_write_frame:frame];
               frame_count++;

           }else{
               if ([FrameData shareFrameData].decodeStatus == 2) {
                   [encoderVideo encode_write_frame:NULL];
                   break;
               }
               [NSThread sleepForTimeInterval:0.01];
           }
        }
        [encoderVideo close_output_video];

    });
}


@end
