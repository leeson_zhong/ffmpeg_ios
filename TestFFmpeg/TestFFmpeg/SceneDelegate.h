//
//  SceneDelegate.h
//  TestFFmpeg
//
//  Created by leeson zhong on 2020/7/23.
//  Copyright © 2020 octant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

