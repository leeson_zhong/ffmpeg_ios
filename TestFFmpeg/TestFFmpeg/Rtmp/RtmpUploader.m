//
//  RtmpUploader.m
//  TestFFmpeg
//
//  Created by leeson zhong on 2020/8/11.
//  Copyright © 2020 octant. All rights reserved.
//

#import "RtmpUploader.h"

@interface RtmpUploader(){
    //解码的信息，编码器设置时使用
    AVFormatContext *ifmt_ctx;
    AVCodecContext *idec_ctx;
    int i_video_stream_index;
    //编码信息
    AVFormatContext *ofmt_ctx;
    AVCodecContext *enc_ctx;
}

@end

@implementation RtmpUploader

-(void)setDecoderAVFormatContext:(AVFormatContext *)formatContext AVCodecContext:(AVCodecContext *)codecContext VideoStreamIndex:(int)index{
    ifmt_ctx = formatContext;
    idec_ctx = codecContext;
    i_video_stream_index = index;
}


-(int)open_output_file:(const char *)filename
{
    AVStream *out_stream;
    AVStream *in_stream;
    AVCodec *encoder;
    int ret;
    
    avformat_network_init();
    
    char *ofmt_name = NULL;
    if (strstr(filename, "rtmp://") != NULL) {
        ofmt_name = "flv";
    }
    else if (strstr(filename, "udp://") != NULL) {
        ofmt_name = "mpegts";
    }
    else {
        ofmt_name = NULL;
    }

    ofmt_ctx = NULL;
    avformat_alloc_output_context2(&ofmt_ctx, NULL, ofmt_name, filename);
    if (!ofmt_ctx) {
        av_log(NULL, AV_LOG_ERROR, "Could not create output context\n");
        return AVERROR_UNKNOWN;
    }

    out_stream = avformat_new_stream(ofmt_ctx, NULL);
    if (!out_stream) {
        av_log(NULL, AV_LOG_ERROR, "Failed allocating output stream\n");
        return AVERROR_UNKNOWN;
    }
    
    in_stream = ifmt_ctx->streams[i_video_stream_index];

    if (idec_ctx->codec_type == AVMEDIA_TYPE_VIDEO
            || idec_ctx->codec_type == AVMEDIA_TYPE_AUDIO) {
        /* in this example, we choose transcoding to same codec */
        encoder = avcodec_find_encoder(idec_ctx->codec_id);
        if (!encoder) {
            av_log(NULL, AV_LOG_FATAL, "Necessary encoder not found\n");
            return AVERROR_INVALIDDATA;
        }
        enc_ctx = avcodec_alloc_context3(encoder);
        if (!enc_ctx) {
            av_log(NULL, AV_LOG_FATAL, "Failed to allocate the encoder context\n");
            return AVERROR(ENOMEM);
        }

        /* In this example, we transcode to same properties (picture size,
         * sample rate etc.). These properties can be changed for output
         * streams easily using filters */
        if (idec_ctx->codec_type == AVMEDIA_TYPE_VIDEO) {
            //设置和源文件一样的码流
            enc_ctx->bit_rate = idec_ctx->bit_rate;
            enc_ctx->height = idec_ctx->height;
            enc_ctx->width = idec_ctx->width;
            enc_ctx->sample_aspect_ratio = idec_ctx->sample_aspect_ratio;
            /* take first format from list of supported formats */
            /*if (encoder->pix_fmts)
                enc_ctx->pix_fmt = encoder->pix_fmts[0];
            else*///leeson,use the same pix_fmt ,because frame format is the same
            
            enc_ctx->pix_fmt = idec_ctx->pix_fmt;
            /* video time_base can be set to whatever is handy and supported by encoder */
            if(idec_ctx->framerate.num==0){
            //leeson,I do not know why dec_ctx->framerate unknown
            //maybe because i use h264_mediacodec to decode
                enc_ctx->time_base = av_inv_q(in_stream->r_frame_rate);
                enc_ctx->framerate = in_stream->r_frame_rate;
            }else{
                enc_ctx->time_base = av_inv_q(idec_ctx->framerate);
                enc_ctx->framerate = idec_ctx->framerate;
            }
            
            /* emit one intra frame every ten frames
                * check frame pict_type before passing frame
                * to encoder, if frame->pict_type is AV_PICTURE_TYPE_I
                * then gop_size is ignored and the output of encoder
                * will always be I frame irrespective to gop_size
                */
            if (idec_ctx->gop_size>0) {
                enc_ctx->gop_size = idec_ctx->gop_size;
                enc_ctx->max_b_frames = idec_ctx->max_b_frames;
            }else{
                enc_ctx->gop_size = 10;
                enc_ctx->max_b_frames = 1;
            }
           
        } else {
            enc_ctx->sample_rate = idec_ctx->sample_rate;
            enc_ctx->channel_layout = idec_ctx->channel_layout;
            enc_ctx->channels = av_get_channel_layout_nb_channels(enc_ctx->channel_layout);
            /* take first format from list of supported formats */
            enc_ctx->sample_fmt = encoder->sample_fmts[0];
            enc_ctx->time_base = (AVRational){1, enc_ctx->sample_rate};
        }
        if (encoder->id == AV_CODEC_ID_H264){
            av_opt_set(enc_ctx->priv_data, "preset", "slow", 0);
        }

        if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
            enc_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;

        /* Third parameter can be used to pass settings to encoder */
        av_log(NULL, AV_LOG_INFO, "in_stream->r_frame_rate.num=%d,den=%d\n",in_stream->r_frame_rate.num,in_stream->r_frame_rate.den);
        av_log(NULL, AV_LOG_INFO,"AVMEDIA_TYPE_VIDEO=%d,dec_ctx->codec_type=%d,dec_ctx->pix_fmt=%d,dec_ctx->gop_size=%d\n",AVMEDIA_TYPE_VIDEO,idec_ctx->codec_type,idec_ctx->pix_fmt,idec_ctx->gop_size);
        av_log(NULL, AV_LOG_INFO, "enc_ctx.time_base.num=%d,den=%d,dec_ctx->framerate.num=%d,den=%d,dec_ctx->time_base.num=%d,den=%d\n",enc_ctx->time_base.num,enc_ctx->time_base.den,idec_ctx->framerate.num,idec_ctx->framerate.den,idec_ctx->time_base.num,idec_ctx->time_base.den);
        ret = avcodec_open2(enc_ctx, encoder, NULL);
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Cannot open video encoder for stream \n");
            return ret;
        }
        ret = avcodec_parameters_from_context(out_stream->codecpar, enc_ctx);
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Failed to copy encoder parameters to output stream \n");
            return ret;
        }

        out_stream->time_base = in_stream->time_base;

    } else if (idec_ctx->codec_type == AVMEDIA_TYPE_UNKNOWN) {
        av_log(NULL, AV_LOG_FATAL, "Elementary stream  is of unknown type, cannot proceed\n");
        return AVERROR_INVALIDDATA;
    } else {
        /* if this stream must be remuxed */
        ret = avcodec_parameters_copy(out_stream->codecpar, in_stream->codecpar);
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Copying parameters for stream failed\n");
            return ret;
        }
        out_stream->time_base = in_stream->time_base;
    }


    av_dump_format(ofmt_ctx, 0, filename, 1);

    if (!(ofmt_ctx->oformat->flags & AVFMT_NOFILE)) {
        ret = avio_open(&ofmt_ctx->pb, filename, AVIO_FLAG_WRITE);
        if (ret < 0) {
            av_log(NULL, AV_LOG_ERROR, "Could not open output file '%s'", filename);
            return ret;
        }
    }

    /* init muxer, write output file header */
    ret = avformat_write_header(ofmt_ctx, NULL);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Error occurred when opening output file\n");
        return ret;
    }

    return 0;
}

-(int)encode_write_frame:(AVFrame *)frame{
    if(frame){
        av_log(NULL, AV_LOG_DEBUG, "Send frame->pts= %lld\n", frame->pts);
    }
    int ret = 0;
    AVPacket *enc_pkt = av_packet_alloc();
    if (!enc_pkt)
        av_log(NULL, AV_LOG_ERROR, "Error alloc packet\n");


   ret = avcodec_send_frame(enc_ctx, frame);
   if (ret < 0) {
       av_log(NULL, AV_LOG_ERROR, "Error sending a frame for encoding\n");
       return ret;
   }

   while (ret >= 0) {
       ret = avcodec_receive_packet(enc_ctx, enc_pkt);
       if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
           return 0;
       else if (ret < 0) {
           av_log(NULL, AV_LOG_ERROR, "Error during encoding\n");
           return ret;
       }

     /* prepare packet for muxing */
        //enc_pkt->stream_index = stream_index;
       av_log(NULL, AV_LOG_INFO, "before rescale_ts,enc_pkt->pts=%lld\n",enc_pkt->pts);
        av_packet_rescale_ts(enc_pkt,enc_ctx->time_base,ofmt_ctx->streams[0]->time_base);

       av_log(NULL, AV_LOG_INFO, "write frame,enc_pkt->pts=%lld\n",enc_pkt->pts);
       NSLog(@"rite frame,enc_pkt->pts=%lld",enc_pkt->pts);
        /* mux encoded frame */
        ret = av_interleaved_write_frame(ofmt_ctx, enc_pkt);

       av_packet_unref(enc_pkt);
   }

    return ret;
}

-(void)close_output_video{
    av_write_trailer(ofmt_ctx);
    if (ofmt_ctx && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
        avio_closep(&ofmt_ctx->pb);
    avformat_free_context(ofmt_ctx);
}

-(AVFrame *)alloc_frame:(int) pix_fmt wdith:(int) width height:(int) height
{
    AVFrame *frame = av_frame_alloc();
    if (!frame) {
        fprintf(stderr, "Could not allocate video frame\n");
        return NULL;
    }
    frame->format = pix_fmt;
    frame->width  = width;
    frame->height = height;

    int ret = av_frame_get_buffer(frame, 0);
    if (ret < 0) {
        fprintf(stderr, "Could not allocate the video frame data\n");
        return NULL;
    }
    return frame;
}

-(BOOL) fillYuvDataToAVFrame:(AVFrame *)pFrame YuvData:(uint8_t *)yuvData Pts:(int)pts
{
    /* make sure the frame data is writable */
    int ret = av_frame_make_writable(pFrame);
    if (ret < 0)
        return NO;
    pFrame->pts = pts;
    
    if(pFrame->format == AV_PIX_FMT_YUV420P){//yyyyyyyy uu vv
        int t_frameWidth = pFrame->width;
        int t_frameHeight = pFrame->height;
        int t_yPerRowBytes = pFrame->linesize[0];
        int t_uPerRowBytes = pFrame->linesize[1];
        int t_vPerRowBytes = pFrame->linesize[2];

        for(int i = 0;i< t_frameHeight ;i++)
        {
            memcpy((char*)(pFrame->data[0]+i*t_yPerRowBytes),yuvData+i*t_frameWidth,t_frameWidth);
        }

        for(int i = 0;i< t_frameHeight/2 ;i++)
        {
            memcpy((char*)(pFrame->data[1]+i*t_uPerRowBytes),yuvData+t_frameHeight*t_frameWidth+i*t_frameWidth/2,t_frameWidth/2);
        }

        for(int i = 0;i< t_frameHeight/2 ;i++)
        {
            memcpy((pFrame->data[2]+i*t_vPerRowBytes),yuvData+t_frameHeight*t_frameWidth*5/4+i*t_frameWidth/2,t_frameWidth/2);
        }

    }else if(pFrame->format == AV_PIX_FMT_NV12){//yyyyyyyy uv uv
         int t_frameWidth = pFrame->width;
         int t_frameHeight = pFrame->height;
         int t_yPerRowBytes = pFrame->linesize[0];
         int t_uvPerRowBytes = pFrame->linesize[1];


         for(int i = 0;i< t_frameHeight ;i++)
         {
             memcpy((char*)(pFrame->data[0]+i*t_yPerRowBytes),yuvData+i*t_frameWidth,t_frameWidth);
         }

         for(int i = 0;i< t_frameHeight/2 ;i++)
         {
             memcpy((char*)(pFrame->data[1]+i*t_uvPerRowBytes),yuvData+t_frameHeight*t_frameWidth+i*t_frameWidth,t_frameWidth);
         }


        }else if(pFrame->format == AV_PIX_FMT_NV21){//yyyyyyyy vu vu
          int t_frameWidth = pFrame->width;
          int t_frameHeight = pFrame->height;
          int t_yPerRowBytes = pFrame->linesize[0];
          int t_uvPerRowBytes = pFrame->linesize[1];

          for(int i = 0;i< t_frameHeight ;i++)
          {
              memcpy((char*)(pFrame->data[0]+i*t_yPerRowBytes),yuvData+i*t_frameWidth,t_frameWidth);
          }

          for(int i = 0;i< t_frameHeight/2 ;i++)
          {
              memcpy((char*)(pFrame->data[1]+i*t_uvPerRowBytes),yuvData+t_frameHeight*t_frameWidth+i*t_frameWidth,t_frameWidth);
          }

        }
    return YES;

}

-(void)free_frame:(AVFrame *)pFrame{
    av_frame_free(&pFrame);
}



@end
