//
//  RtmpUploader.h
//  TestFFmpeg
//
//  Created by leeson zhong on 2020/8/11.
//  Copyright © 2020 octant. All rights reserved.
//

#import <Foundation/Foundation.h>

#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>

NS_ASSUME_NONNULL_BEGIN

@interface RtmpUploader : NSObject

-(void)setDecoderAVFormatContext:(AVFormatContext *)formatContext AVCodecContext:(AVCodecContext *)codecContext VideoStreamIndex:(int)index;
-(int)open_output_file:(const char *)filename;
-(int)encode_write_frame:(AVFrame *)frame;
-(void)close_output_video;
-(AVFrame *)alloc_frame:(int) pix_fmt wdith:(int) width height:(int) height;
-(BOOL) fillYuvDataToAVFrame:(AVFrame *)pFrame YuvData:(uint8_t *)yuvData Pts:(int)pts;
-(void)free_frame:(AVFrame *)pFrame;

@end

NS_ASSUME_NONNULL_END
