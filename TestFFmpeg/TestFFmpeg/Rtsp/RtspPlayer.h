//
//  RtspPlayer.h
//  TestFFmpeg
//
//  Created by leeson zhong on 2020/8/11.
//  Copyright © 2020 octant. All rights reserved.
//

#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@interface RtspPlayer : NSObject

-(int)startPlay:(NSString *) videoPath;

@end

NS_ASSUME_NONNULL_END
