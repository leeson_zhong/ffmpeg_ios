//
//  FrameData.h
//  TestFFmpeg
//
//  Created by leeson zhong on 2020/7/28.
//  Copyright © 2020 octant. All rights reserved.
//
/**
 测试代码
 [[FrameData shareFrameData] initYuvDataQueueNum:10 Len:10];
 for (int i = 0; i<10; i++) {
     NSMutableData *data = [[FrameData shareFrameData] offerYuvData];
     if (data) {
         Byte* byteArr = [data mutableBytes];
         byteArr[0] = i;
     }else{
         break;
     }
     
 }
 
 while (true) {
     NSMutableData *data = [[FrameData shareFrameData] pollYuvData];
     if (data) {
         Byte* byteArr = [data mutableBytes];
         NSLog(@"pollYuvData byteArr[0]=%d",byteArr[0]);
     }else{
         break;
     }
 }
 */

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FrameData : NSObject

@property(nonatomic,assign) int decodeStatus;
@property(nonatomic,assign) int queueNum;
@property(nonatomic,assign) BOOL encode;

+(instancetype) shareFrameData;

-(void)initYuvDataQueueNum:(int)num Len:(int)len;
-(NSMutableData *)pollYuvData;
-(NSMutableData *)offerYuvData;

@end

NS_ASSUME_NONNULL_END
