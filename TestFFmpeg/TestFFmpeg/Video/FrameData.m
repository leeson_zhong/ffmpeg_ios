//
//  FrameData.m
//  TestFFmpeg
//
//  Created by leeson zhong on 2020/7/28.
//  Copyright © 2020 octant. All rights reserved.
//

#import "FrameData.h"
#import <pthread.h>

/**
 数组实现循环队列时，始终会有一个空余的位置预留着，作为一个判决队列已满的条件
 */

@interface FrameData(){
    NSMutableArray *yuvDataQueue;
    int firstPointer;//指向可以用的
    int lastPointer;//指向空闲的
    __block pthread_mutex_t mutex;
}

@end

@implementation FrameData

static FrameData *_instance = nil;

+(void) load{
    _instance = [[self alloc] init];
}

+(instancetype)shareFrameData{
    return _instance;
}

+(instancetype)alloc{
    if (_instance) {
        NSException *excep = [NSException exceptionWithName:@"NSInternalInconsistencyException" reason:@"There can only be one FrameData instance." userInfo:nil];
        [excep raise];
    }
    return [super alloc];
}

-(instancetype)init{
    self = [super init];
    if (self) {
        self.decodeStatus = 0;
        self.queueNum = 10;
        self.encode = NO;
        yuvDataQueue = [[NSMutableArray alloc] initWithCapacity:self.queueNum];
        pthread_mutex_init(&mutex, NULL);
    }
    return self;
}

-(void)initYuvDataQueueNum:(int)num Len:(int)len{
    self.queueNum = num;
    if (yuvDataQueue) {
        [yuvDataQueue removeAllObjects];
    }
    yuvDataQueue = [[NSMutableArray alloc] initWithCapacity:self.queueNum];
    firstPointer = 0;
    lastPointer = 0;
    for (int i=0; i<self.queueNum; i++) {
        NSMutableData* aData = [NSMutableData dataWithLength:len];
        [yuvDataQueue addObject:aData];
    }
    
}

-(NSMutableData *)pollYuvData{
    NSMutableData* result = nil;
    pthread_mutex_lock(&mutex);
    if (firstPointer!=lastPointer) {
        result = [yuvDataQueue objectAtIndex:firstPointer];
        firstPointer ++;
        firstPointer = firstPointer % self.queueNum;
    }
    pthread_mutex_unlock(&mutex);
    return result;
}

-(NSMutableData *)offerYuvData{
    NSMutableData* result = nil;
    pthread_mutex_lock(&mutex);
    int eleCount = (lastPointer - firstPointer + self.queueNum) % self.queueNum;
    if (eleCount<self.queueNum-1) {
        result = [yuvDataQueue objectAtIndex:lastPointer];
        lastPointer ++;
        lastPointer = lastPointer % self.queueNum;
    }
    pthread_mutex_unlock(&mutex);
    return result;
}

@end
