//
//  VideoInfo.m
//  TestFFmpeg
//
//  Created by leeson zhong on 2020/7/28.
//  Copyright © 2020 octant. All rights reserved.
//

#import "VideoInfo.h"

@implementation VideoInfo

static VideoInfo *_instance = nil;

+(void) load{
    _instance = [[self alloc] init];
}

+(instancetype)shareVideoInfo{
    return _instance;
}

+(instancetype)alloc{
    if (_instance) {
        NSException *excep = [NSException exceptionWithName:@"NSInternalInconsistencyException" reason:@"There can only be one VideoInfo instance." userInfo:nil];
        [excep raise];
    }
    return [super alloc];
}

-(instancetype)init{
    self = [super init];
    if (self) {
       
    }
    return self;
}


@end
