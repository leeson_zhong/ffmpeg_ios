//
//  VideoInfo.h
//  TestFFmpeg
//
//  Created by leeson zhong on 2020/7/28.
//  Copyright © 2020 octant. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Foundation/Foundation.h>
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
#include <libswscale/swscale.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoInfo : NSObject

@property(nonatomic,assign) AVFormatContext *fmt_ctx;
@property(nonatomic,assign) AVCodecContext *dec_ctx;
@property(nonatomic,assign) int video_stream_index;

+(instancetype) shareVideoInfo;

@end

NS_ASSUME_NONNULL_END
