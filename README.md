#  下载资源

- 下载[ffmpeg编译脚本](https://github.com/kewlbear/FFmpeg-iOS-build-script)

- 下载[ffmpeg代码](https://github.com/FFmpeg/FFmpeg)，根据上面脚本github的README.md测试的是FFmpeg4.2的，所以下载对应版本的code。`git clone https://github.com/FFmpeg/FFmpeg.git --branch release/4.2`

- 把下载的**build-ffmpeg.sh**和存放ffmpeg代码的**FFmpeg**文件夹放到同一个目录下

- 用记事本打开build-ffmpeg.sh可以看到

  ```
  FF_VERSION="4.2"
  ```

  `SOURCE="ffmpeg-$FF_VERSION"`

  因此把存放ffmpeg代码的文件夹改成**ffmpeg-4.2**

- ffmpeg脚本的gitee地址https://gitee.com/leesonzhong/FFmpeg-iOS-build-script

- ffmpeg代码的gitee地址https://gitee.com/leesonzhong/FFmpeg

# 编译ffmpeg

- 运行脚本。`$ ./build-ffmpeg.sh`
- 中途可能会尝试下载[gas-preprocessor](https://github.com/libav/gas-preprocessor)和[yasm](brew install yasm)
- 编译成功后可以看到FFmpeg-iOS文件夹，里面包含了include和lib文件夹，分别存储了头文件和库文件。

# 使用ffmpeg

- 拷贝FFmpeg-iOS文件到工程目录下并添加到工程
- 添加**libz.tbd**、**libbz2.tbd**、**libiconv.tbd**和**VideoToolbox.framework**(我用的是xcode11.3.1，第一次添加tbd时没有成功，再添加一次成功。如果**Build Phases** -> **Link Binary With Libraries**没有看到添加的库，重新添加一遍，还不行重启工程再试)
- **Build Settings** -> **System Header Search Paths**添加**FFmpeg-iOS/include**的路径，可以通过拖拉左边目录结构栏的include目录到添加栏快速填写路径
- 解码时编译发现还需要添加**AudioToolbox.framework**和**CoreMedia.framework**

# ffmpeg常用数据结构

![o_ffmpeg](./o_ffmpeg.jpg)



# ffmpeg进行解码

- 打开视频文件获取AVFormatContext的变量fmt_ctx

    if ((ret = avformat_open_input(&fmt_ctx, filename, NULL, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open input file\n");
        return ret;
    }
- 根据AVFormatContext的变量fmt_ctx获取视频流的索引video_stream_index和用于该视频流的解码器AVCodec的变量dec

    if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
        return ret;
    }
    
    /* select the video stream */
    ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot find a video stream in the input file\n");
        return ret;
    }
    video_stream_index = ret;
- 根据AVCodec的变量dec创建AVCodecContext变量dec_ctx。根据AVFormatContext的变量fmt_ctx和视频流的索引video_stream_index得到对应视频流的解码属性并设置给AVCodecContext的变量dec_ctx。最后通过AVCodec的变量dec来初始化AVCodecContext的变量dec_ctx。

    /* create decoding context */
    dec_ctx = avcodec_alloc_context3(dec);
    if (!dec_ctx)
    	return AVERROR(ENOMEM);
    avcodec_parameters_to_context(dec_ctx, fmt_ctx->streams[video_stream_index]->codecpar);
    av_opt_set_int(dec_ctx, "refcounted_frames", 1, 0);
    
    /* init the video decoder */
    if ((ret = avcodec_open2(dec_ctx, dec, NULL)) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
        return ret;
    }
- 通过AVFormatContext的变量fmt_ctx得到AVPacket的变量packet。AVPacket存储的是编码数据，比如H264、AAC等。

    if ((ret = av_read_frame(fmt_ctx, &packet)) < 0)
    	break;
- 把AVPacket的变量packet丢给AVCodecContext的变量dec_ctx进行解码，然后获取解码得到的AVFrame的变量frame。AVFrame存储的是像素数据，比如YUV、RGB、PCM等。

    if (packet.stream_index == video_stream_index) {
            ret = avcodec_send_packet(dec_ctx, &packet);
            if (ret < 0) {
              av_log(NULL, AV_LOG_ERROR, "Error while sending a packet to the decoder\n");
              break;
            } 
            while (ret >= 0) {
                ret = avcodec_receive_frame(dec_ctx, frame);
                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                    break;
                } else if (ret < 0) {
                    av_log(NULL, AV_LOG_ERROR, "Error while receiving a frame from the decoder\n");
                    goto end;
                }
- 通过avcodec_receive_frame得到AVFrame的变量frame后，使用变量frame结束后记得释放

`av_frame_unref(frame);`

- 通过av_read_frame得到AVPacket的变量packet后，使用变量packet结束后记得释放

`av_packet_unref(&packet);`

# ffmpeg编码

- 根据要输出的视频路径得到AVFormatContext的变量ofmt_ctx

```
    avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, filename);
    if (!ofmt_ctx) {
        av_log(NULL, AV_LOG_ERROR, "Could not create output context\n");
        return AVERROR_UNKNOWN;
    }
```



- 通过AVFormatContext的变量ofmt_ctx申请一个新的AVStream变量out_stream，用于存放视频流的

```
		out_stream = avformat_new_stream(ofmt_ctx, NULL);
    if (!out_stream) {
        av_log(NULL, AV_LOG_ERROR, "Failed allocating output stream\n");
        return AVERROR_UNKNOWN;
    }
```

- 获取编码器，可以通过avcodec_find_encoder或avcodec_find_encoder_by_name。我这边是根据解码的信息来获取编码器AVCodec的变量encoder。

```
encoder = avcodec_find_encoder(dec_ctx->codec_id);
if (!encoder) {
		av_log(NULL, AV_LOG_FATAL, "Necessary encoder not found\n");
		return AVERROR_INVALIDDATA;
}
```

- 根据AVCodec的变量encoder可以创建AVCodecContext变量enc_ctx

```
enc_ctx = avcodec_alloc_context3(encoder);
if (!enc_ctx) {
		av_log(NULL, AV_LOG_FATAL, "Failed to allocate the encoder context\n");
		return AVERROR(ENOMEM);
}
```

- 设置AVCodecConext变量enc_ctx的属性，我这边是根据解码的信息来设置的。

```
if (dec_ctx->codec_type == AVMEDIA_TYPE_VIDEO) {
    //设置和源文件一样的码流
    enc_ctx->bit_rate = dec_ctx->bit_rate;
    enc_ctx->height = dec_ctx->height;
    enc_ctx->width = dec_ctx->width;
    enc_ctx->sample_aspect_ratio = dec_ctx->sample_aspect_ratio;
    /* take first format from list of supported formats */
    enc_ctx->pix_fmt = dec_ctx->pix_fmt;
    /* video time_base can be set to whatever is handy and supported by encoder */
    if(dec_ctx->framerate.num==0){
        //leeson,I do not know why dec_ctx->framerate unknown
        enc_ctx->time_base = av_inv_q(in_stream->r_frame_rate);
    }else{
        enc_ctx->time_base = av_inv_q(dec_ctx->framerate);
    }
}

if (ofmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)
		enc_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
```

- 设置编码质量为slow。medium(默认值）时，编码的视频播放发现有些帧图像显示出问题。编码时间变长，但是质量变好。

  ```
  if (codec->id == AV_CODEC_ID_H264)
          av_opt_set(c->priv_data, "preset", "slow", 0);
  ```

  

- 初始化AVCodecConext变量enc_ctx

```
ret = avcodec_open2(enc_ctx, encoder, NULL);
if (ret < 0) {
		av_log(NULL, AV_LOG_ERROR, "Cannot open video encoder for stream \n");
		return ret;
}
```

- 设置AVStream的变量out_stream的属性

```
ret = avcodec_parameters_from_context(out_stream->codecpar, enc_ctx);
if (ret < 0) {
		av_log(NULL, AV_LOG_ERROR, "Failed to copy encoder parameters to output stream \n");
		return ret;
}

out_stream->time_base = enc_ctx->time_base;
```

- 向输出的视频文件写入输出格式的详细信息

```
av_dump_format(ofmt_ctx, 0, filename, 1);
```

- 打开输出视频文件，并把文件指针赋值给AVFormatContext的变量ofmt_ctx

```
if (!(ofmt_ctx->oformat->flags & AVFMT_NOFILE)) {
    ret = avio_open(&ofmt_ctx->pb, filename, AVIO_FLAG_WRITE);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Could not open output file '%s'", filename);
        return ret;
    }
}
```

- 通过AVFormatContext的变量ofmt_ctx写入视频头

```
ret = avformat_write_header(ofmt_ctx, NULL);
if (ret < 0) {
    av_log(NULL, AV_LOG_ERROR, "Error occurred when opening output file\n");
    return ret;
}
```

- 把存储了像素数据的AVFrame的变量给AVCodecContext的变量enc_ctx进行编码

```
ret = avcodec_send_frame(enc_ctx, frame);
if (ret < 0) {
	av_log(NULL, AV_LOG_ERROR, "Error sending a frame for encoding\n");
	return ret;
}
```

- 从AVCodecContext的变量enc_ctx获取到编码数据包AVPacket的变量enc_pkt，把AVPacket的变量enc_pkt的时间字段转换成基于视频流时间基，通过AVFormatContext的变量ofmt_ctx写到视频文件中，最后释放AVPacket的变量enc_pkt。

```
while (ret >= 0) {
  ret = avcodec_receive_packet(enc_ctx, enc_pkt);
  if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
  	return 0;
  else if (ret < 0) {
    av_log(NULL, AV_LOG_ERROR, "Error during encoding\n");
    return ret;
  }

	/* prepare packet for muxing */
	av_packet_rescale_ts(enc_pkt,enc_ctx->time_base,ofmt_ctx->streams[0]->time_base);
	
	/* mux encoded frame */
	ret = av_interleaved_write_frame(ofmt_ctx, enc_pkt);

	av_packet_unref(enc_pkt);
}
```

- 视频数据写完后，写上视频尾，然后关闭视频文件并释放AVFormatContext的变量ofmt_ctx。

```
av_write_trailer(ofmt_ctx);
if (ofmt_ctx && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
		avio_closep(&ofmt_ctx->pb);
avformat_free_context(ofmt_ctx);
```

# FFmpeg开发之pts、dts、time_base概念理解

- PTS：Presentation Time Stamp，反映帧什么时候开始显示。PTS 主要用于度量解码后的视频帧什么时候被显示出来。
  DTS：Decode Time Stamp，反映数据流什么时候开始解码。DTS 主要是标识读入内存中的Bit流在什么时候开始送入解码器中进行解码。

  time_base：Time Base，时间基，用来度量时间的。如果把1秒分为25等份，你可以理解就是一把尺，那么每一格表示的就是1/25秒。此时的time_base={1，25}。如果你是把1秒分成90000份，每一个刻度就是1/90000秒，此时的time_base={1，90000}。所谓时间基表示的就是每个刻度是多少秒。此时你应该不难理解 pts*av_q2d(time_base)才是帧的显示时间戳。

- 不同的封装格式的timebase是不一样的。另外，整个转码过程中，不同数据状态对应的时间基也是不一致的。拿MPEG-TS封装格式25fps来说(主要是视频)，非压缩时候的数据（即YUV数据或者其它格式数据），在FFmpeg中对应的结构体为AVFrame，它的时间基为AVCodecContext 的time_base，AVRational{1,25}。 压缩后的数据（对应的结构体为AVPacket）对应的时间基为AVStream的time_base，AVRational{1,90000}。 因为数据状态不同，时间基不一样，所以我们必须转换，在1/25时间刻度下占10格，在1/90000下是占多少格，这就是pts的转换
- 根据pts来计算一桢在整个视频中的时间位置： timestamp(秒) = pts * av_q2d(st->time_base)。
- duration和pts单位一样，duration表示当前帧的持续时间占多少格。或者理解是两帧的间隔时间是占多少格。 time(秒) = st->duration * av_q2d(st->time_base)
- FFmpeg内部的时间戳 = AV_TIME_BASE * time(秒)，其中 AV_TIME_BASE_Q = 1 / AV_TIME_BASE
- av_rescale_q(int64_t a, AVRational bq, AVRational cq) 函数，这个函数的作用是计算 a * bq / cq 来把时间戳从一个时间基调整到另外一个时间基。在进行时间基转换的时候，应该优先选择这个函数，因为它可以避免溢出的情况发生。函数表示在bq下的占a个格子，在cq下是多少。
- AVCodecContext的time_base一般是framerate（帧率）的倒数，所以AVFrame的pts刚好表示为第几帧（从第0帧开始）

# ios沙盒的文件怎么查看

- 使用itunes应用查看（新版本mac只有叫音乐的，无法管理iphone应用）

- 打开xcode，在上方菜单栏**Window**->**Devices and Simulators**

- 选择对应用，然后选择Download Containers，等待下载完成提示

  ![sandbox](./sandbox.png)

- 右键点击下载的包，然后选择显示包内容，把需要的文件夹或文件拉出来。

# FFMpeg处理RTMP流有两种方式

- FFMpeg处理RTMP流有两种方式:

一个是使用自带的RTMP代码功能；
 一个是使用第三方库librtmp；
下面就这两种方式的一些使用和差异做了个总结；

- 一、自带RTMP代码功能
  FFmpeg自带的RTMP代码只支持RTMP协议，不支持rtmpt,rtmpe,rtmpte和rtmps协议；
  命令行设置如下:

  1. 将RTMP流原样保存成文件
     \# ./ffmpeg -i rtmp://192.168.1.11:1935/live/teststream -acodec copy -vcodec copy -f flv -y test.flv

  2. 将RTMP流转码保存成文件
     \# ./ffmpeg -i rtmp://192.168.1.11:1935/live/teststream -acodec ... -vcodec ... -f mp4 -y test.mp4

  3. 将RTMP流转码后再以RTMP流的方式推送到RTMP流服务器
     \# ./ffmpeg -i rtmp://192.168.1.11:1935/live/teststream -acodec ... -vcodec ... -f flv rtmp://10.2.11.111/live/newstream


  NOTE:
  FFMpeg自带RTMP代码只支持RTMP流格式如:
  rtmp://server:port/app/stream_name (eg: rtmp://192.168.1.11:80/live/test)
  不支持RTMP流格式如:
  rtmp://192.168.1.11:80/live/app/test
  要想支持这种格式的RTMP流，就需要更专业和强大的每三方库librtmp; 

- 二、第三方库librtmp
  如何让FFMpeg链接该库可以参见文章:
  http://blog.csdn.net/fireroll/article/details/8607955

  这样FFMpeg就可以支持rtmp://, rtmpt://, rtmpe://, rtmpte://,以及 rtmps://协议了。
  链接了librtmp的FFMpeg接受一个字符串的输入方式，
  如："rtmp://server:port/app/playpath/stream_name live=1 playpath=xxx ..."
  NOTE：引号是必须的;

  1. 保存RTMP直播流原样保存成文件：
     \# ./ffmpeg -i "rtmp://pub1.guoshi.com/live/newcetv1 live=1" -vcodec copy -acodec copy -y cetv1.flv  

  2. 将RTMP流转码后再以RTMP流的方式推送到RTMP流服务器
     \# ./ffmpeg -i "rtmp://192.168.1.11:1935/live/app/teststream live=1" -acodec ... -vcodec ... -f flv rtmp://10.2.11.111/live/newstream

  3. 用ffplay播放RTMP直播流：
     ffplay "rtmp://pub1.guoshi.com/live/newcetv1 live=1" 

  4. 在使用FFMPEG类库进行编程的时候，也是一样的，
     只需要将字符串传递给avformat_open_input()就行了，形如：
     ffplay "rtmp://pub1.guoshi.com/live/newcetv1 live=1"  

     char url[]="rtmp://live.hkstv.hk.lxdns.com/live/hks live=1";  
     avformat_open_input(&pFormatCtx,url,NULL,&avdic)*  

# 8.播放RTSP UDP花屏解决方法。

- 改成tcp，但是有些rtsp源不支持tcp

  ```plaintext
  av_dict_set(&options, "rtsp_transport", "tcp", 0);  //以tcp方式打开
  ```

- 把buffsize加大

  ```plaintext
  av_dict_set(&options, "buffer_size", "4096000", 0);
  ```

- 在ffmpeg源代码找到udp.c，应该在libavformat目录下，加大UDP_MAX_PKT_SIZE的值，重新编译ffmpeg库

  ```plaintext
  #define UDP_MAX_PKT_SIZE 65536*10
  ```

- 丢弃花屏的帧，参考网址：https://blog.csdn.net/sz76211822/article/details/87797475（ffmpeg播放rtsp视频流花屏解决办法)

  1.设全局变量 在丢包或解码出错时将全局变量置为不同的值 最后在使用的地方根据全局变量的值来判断该帧是否完整 全局变量可在ffmpeg任意.h文件中设置 我是在avcodec.h中设置的。这里我发现有一个问题，那就是编译多个动态库，而这些库都使用这个全局变量，所以编译会报错。所以分别为每个库定义一个全局变量，rtpdec.c是属于libavformat库，而其它几个c文件属于libavcodec库。本来想用bool类型，但编译错误，只好用uint8_t类型.

  ```plaintext
  //avcodec.h
  extern uint8_t avformat_frame_is_normal;
  extern uint8_t avcodec_frame_is_normal;
  
  //rtpdec.c
  uint8_t avformat_frame_is_normal;
  
  //error_resilience.c
  uint8_t avcodec_frame_is_normal;
  ```

  2.修改rtpdec.c文件包含 missed %d packets的地方，这里出现丢包，需作标记。注意要添加大括号

  ```plaintext
      if (!has_next_packet(s)){
      		avformat_frame_is_normal = 0;
          av_log(s->ic, AV_LOG_WARNING,
                 "RTP: missed %d packets\n", s->queue->seq - s->seq - 1);
  	}
  ```

  3.修改error_resilience.c文件 包含concealing %d DC, %d AC, %d MV errors in %c frame的地方。这里出现解包错误，需标记

  ```plaintext
      avcodec_frame_is_normal = 0;
      av_log(s->avctx, AV_LOG_INFO, "concealing %d DC, %d AC, %d MV errors in %c frame\n",
             dc_error, ac_error, mv_error, av_get_picture_type_char(s->cur_pic.f->pict_type));
  ```

  4.修改h264_cavlc.c文件中包含 Invalid level prefix处 这里出错 需标记。查找发现有两处

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "Invalid level prefix\n");
  ```

  修改h264_cavlc.c文件中包含dquant out of range处，出错，需标记

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "dquant out of range (%d) at %d %d\n", dquant, sl->mb_x, sl->mb_y);
  ```

  修改h264_cavlc.c文件中包含corrupted macroblock处，出错，需标记

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "corrupted macroblock %d %d (total_coeff=%d)\n", sl->mb_x, sl->mb_y, total_coeff);
  ```

  修改h264_cavlc.c文件中包含negative number of zero coeffs at处，出错，需标记

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "negative number of zero coeffs at %d %d\n", sl->mb_x, sl->mb_y);
  ```

  修改h264_cavlc.c文件中包含mb_type %d in %c slice too large at %d %d处，出错，徐标记

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "mb_type %d in %c slice too large at %d %d\n", mb_type, av_get_picture_type_char(sl->slice_type), sl->mb_x, sl->mb_y);
  ```

  修改h264_cavlc.c文件中包含cbp too large处，出错，需标记 。查找这里也有两处

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR, "cbp too large (%u) at %d %d\n", cbp, sl->mb_x, sl->mb_y);
  ```

  5.修改error_resilience.c文件中包含Cannot use previous picture in error concealment处，出错，需标记

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(s->avctx, AV_LOG_WARNING, "Cannot use previous picture in error concealment\n");
  ```

  修改error_resilience.c文件中包含Cannot use next picture in error concealment处，出错，需标记

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(s->avctx, AV_LOG_WARNING, "Cannot use next picture in error concealment\n");
  ```

  6.修改h264_parse.c文件中包含out of range intra chroma pred mode处，出错，需标记。（原教程是h264.c，但是没有这个文件，而这些关键log是在h264_parse.c，下面h264_parse.c原教程也是h264.c）

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(logctx, AV_LOG_ERROR,
  	"out of range intra chroma pred mode\n");
  ```

  修改h264_parse.c文件中包含top block unavailable for requested intra mode处，出错，需标记。查找有两处。

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(logctx, AV_LOG_ERROR,
  	"top block unavailable for requested intra mode\n");
  ```

  修改h264_parse.c文件中包含left block unavailable for requested intra mode处，出错，需标记

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(logctx, AV_LOG_ERROR,
  	"left block unavailable for requested intra mode\n");
  ```

  7.修改h264_slice.c文件中包含error while decoding MB处，出错，需标记。查找有两处。

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR,
  	"error while decoding MB %d %d, bytestream %"PTRDIFF_SPECIFIER"\n",
  	sl->mb_x, sl->mb_y,
  	sl->cabac.bytestream_end - sl->cabac.bytestream);
  	
  avcodec_frame_is_normal = 0;
  av_log(h->avctx, AV_LOG_ERROR,
      "error while decoding MB %d %d\n", sl->mb_x, sl->mb_y);
  ```

  8.修改svq3.c文件中包含error while decoding MB处，出错，需标记

  ```plaintext
  avcodec_frame_is_normal = 0;
  av_log(s->avctx, AV_LOG_ERROR,
  	"error while decoding MB %d %d\n", s->mb_x, s->mb_y);
  ```

  9.每一帧处理后或处理前，都需要将全局变量重置。教程没说哪里进行重置，我不太懂ffmpeg源码，所以自己调用ffmpeg方法进行解码的地方进行重置。不知道为什么，如果我在avcodec_send_packet前面重置，花屏时可以在avcodec_receive_frame之后获取到avcodec_frame_is_normal为0。但是如果我在avcodec_receive_frame前面重置，avcodec_receive_frame之后获取到的avcodec_frame_is_normal一直为1。

  ```plaintext
  avcodec_frame_is_normal = 1;
  avformat_frame_is_normal = 1;
  ```

  10.修改完成后，需重新编译ffmpeg源码。可以在使用的时候将全局变量打印出来，会发现一旦有丢包或解码错误现象，全局变量就会有对应的值。这样就可以通过该全局变量来判断该帧是否完整了，从而过滤掉。判断花屏后，需要等待I帧再显示，否则还是有花屏现象。

  ```plaintext
  if(avcodec_frame_is_normal == 0 || avformat_frame_is_normal == 0){
      av_frame_unref(frame);
      need_key_frame = true;
      continue;
  }
  if(need_key_frame){
      if (frame->key_frame == 1){
      	need_key_frame = false;
      }else{
      	av_frame_unref(frame);
      	continue;
      }
  }
  ```

- 看到一个教程说视频流如果无法保证视频流是一帧一帧的发送，是要添加CODEC_FLAG_TRUNCATED。参考网址：https://www.oschina.net/question/436610_51323（用ffmpeg中的avcodec_decode_video解码视频流，注释掉CODEC_FLAG_TRUNCATED，有马赛克？）。

  ```plaintext
      if (mVideoCodec->capabilities&CODEC_CAP_TRUNCATED) {
          mVideoCodecCtx->flags |= CODEC_FLAG_TRUNCATED;
      }
  ```

- 看到关于花屏的解析的教程，感觉好像挺有用，网址：https://blog.csdn.net/zhoubotong2012/article/details/103002257（解码H264视频出现花屏或马赛克的问题）。通过使用av_parser_parse2函数把多个不连续的数据块组成一帧，而不是使用CODEC_FLAG_TRUNCATED。但是发现av_read_frame源代码有调用av_parser_parse2。

# 9.添加缓存队列，把接收packet和解码分成两个线程

- 队列的实现，自定义循环列表，保存avpacket指针的值，创建AVPacketQueue.h和AVPacketQueue.m，头文件AVPacketQueue.h内容如下

  ```plaintext
  //
  //  AVPacketQueue.h
  //  eSky
  //
  //  Created by leeson zhong on 2021/9/10.
  //  Copyright © 2021 octant. All rights reserved.
  //
  
  #import <Foundation/Foundation.h>
  
  #include <libavformat/avformat.h>
  #include <libavcodec/avcodec.h>
  #include <libavutil/opt.h>
  #include <libavutil/imgutils.h>
  #include <libswscale/swscale.h>
  
  #define MAXSIZE 10
  
  NS_ASSUME_NONNULL_BEGIN
  
  @interface AVPacketQueue : NSObject
  
  -(void)initAVPacketQueue;
  -(void)releaseAVPacketQueue;
  //入队，从队尾(rear)入
  -(BOOL)Push:(AVPacket* )val;
  //出队，从队首(front)出
  -(BOOL)Pop:(AVPacket* )packet;
  //获取队列长度
  -(int)GetQueueLen;
  //队列为空则返回true
  -(BOOL)IsEmpty;
  //队列满则返回false
  -(BOOL)IsFull;
  
  @end
  
  NS_ASSUME_NONNULL_END
  
  ```
  
- AVPacketQueue.m实现队列方法，av_packet_alloc创建packet指针，av_copy_packet复制packet指针指向的值，av_packet_free释放packet指针

  ```plaintext
  //
  //  AVPacketQueue.m
  //  eSky
  //
  //  Created by leeson zhong on 2021/9/10.
  //  Copyright © 2021 octant. All rights reserved.
  //
  
  #import "AVPacketQueue.h"
  
  @interface AVPacketQueue(){
      AVPacket* elem[MAXSIZE];
      int front;        //下标可取,指向队头元素
      int rear;            //下标不可取,指向元素应放入的位置
  }
  @end
  
  @implementation AVPacketQueue
  
  -(void)initAVPacketQueue{
      front = 0;
      rear = 0;
      for (int i = 0; i != MAXSIZE; ++i) {
          elem[i] = av_packet_alloc();
      }
  }
  -(void)releaseAVPacketQueue{
      front = 0;
      rear = 0;
      for (int i = 0; i != MAXSIZE; ++i) {
          av_packet_free(&elem[i]);
      }
  }
  
  //入队，从队尾(rear)入
  -(BOOL)Push:(AVPacket* )val{
      //入队之前应该先判断队列是否已经满了
  
      if ([self IsFull])
  
      {
  
          return false;
  
      }
      av_copy_packet(elem[rear], val);
  
      rear = (rear+ 1) % MAXSIZE;    //更新队尾位置
      
      return true;
  }
  
  //出队，从队首(front)出
  -(BOOL)Pop:(AVPacket* )packet{
  //出队之前应该先判断队列是否是空的
  
      if ([self IsEmpty])
  
      {
  
          return false;
  
      }
  
      av_copy_packet(packet, elem[front]);
  
  
      front = (front+ 1) % MAXSIZE;  //更新队头位置
  
      return true;
  }
  
  //获取队列长度
  -(int)GetQueueLen{
      return ((rear - front + MAXSIZE) % MAXSIZE);
  }
  
  //队列为空则返回true
  -(BOOL)IsEmpty{
      return rear == front;
  }
  
  //队列满则返回false
  -(BOOL)IsFull{
      return (rear + 1)%MAXSIZE == front;
  }
  
  -(int)getFront{
      return front;
  }
  -(int)getRear{
      return rear;
  }
  @end
  
  ```

- 在打开RTSP接收packet的线程中，把packet放入队列。我为了不影响延时，满队列后弹出packet后再放入新的packet。使用时记得先初始化

  ```plaintext
          AVPacketQueue *queue;
          queue = [[AVPacketQueue alloc] init];
          [queue initAVPacketQueue];
      
      
      while (stopFlag == false) {
          if ((ret = av_read_frame(fmt_ctx, &packet)) < 0)
              break;
          playing = true;
  
          while (queue.IsFull()){
              AVPacket *queuePacket = av_packet_alloc();
              queue.Pop(queuePacket);
              av_packet_free(&queuePacket);
  //            usleep(1000*10);
          }
          LOGI("before queue.Push, len=%d",queue.GetQueueLen());
          queue.Push(&packet);
  
          av_packet_unref(&packet);
      }
  ```

- 在解码packet的线程中，把packet取出来

  ```plaintext
  AVPacket *packetPtr = av_packet_alloc();
  
      while (stopFlag == false || playing == true){
          if ([queue IsEmpty]){
              usleep(1000*10);
              continue;
          }
          //NSLog(@"before queue.Pop, len=%d",[queue GetQueueLen]);
          int ret = 0;
          if ([queue Pop:packetPtr]){
              if (packetPtr->stream_index == video_stream_index) {
              	//解码
              }
          }
      }
  ```

# 视频帧率

- 帧率有很大用处，可以用来判断视频是否流畅。而且有时可以用来表示连接信号强度，感觉比系统的WiFi强度好用。

- 一开始，我的想法是创建线程，解码时帧数加1，每秒重置一次

- 我在别人代码中看到一种方法，感觉比较简单实用，不需要创建线程。大概思路是创建对比时间变量，解码时判断当前时间和对比时间变量的秒是否相同，如果相同就帧数加1，否则重置。

  ```
  struct timeval nowtime;
  gettimeofday(&nowtime, NULL);
  if (nowtime.tv_sec != _time.tv_sec)
  {
    //NSLog(@"解码视频帧率:   %ld\n", (long)_frameRate);
    decode_fps = _frameRate;
    memcpy(&_time, &nowtime, sizeof(struct timeval));
    _frameRate = 1;
  }
  else
  {
  	_frameRate++;
  }
  ```

